//
//  statusViewController.swift
//  PrivacyBeacon
//
//  Created by Student on 2014-11-22.
//  Copyright (c) 2014 University of Ontario Institute of Technology. All rights reserved.
//

import UIKit

class statusViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getState()
        
        statusText.text = "Your privacy setting is \(privacySetting).\r\r Your privacy rating is \r\(privacyRating)"
        
        consentText.text = "Consent for Collection: \(collectionConsent)\rConsent for Use: \(useConsent)\rConsent for Disclosure: \(disclosureConsent)"
        
        stateText.text = "Your Privacy State is currently\r\(privacyState) : \(privacyName)\r\r\"\(privacyDescription)\""

        if surveyComplete == false {
            statusText.text = "You have not yet completed the Privacy Survey."
            consentText.text = "No consent to any third-party will be granted until the survey has been completed."
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getState() {
        switch privacyState {
        case 1:
            privacyName = "Private"
            privacyDescription = "Existence is unknown"
        case 2:
            privacyName = "Unidentified"
            privacyDescription = "No identity data"
        case 3:
            privacyName = "Anonymous"
            privacyDescription = "Limited identity information"
        case 4:
            privacyName = "Masked"
            privacyDescription = "Linkages to identity are concealed"
        case 5:
            privacyName = "De-identified"
            privacyDescription = "Non-specific identity information is known"
        case 6:
            privacyName = "Pseudonymous"
            privacyDescription = "Identity data could apply to multiple persons"
        case 7:
            privacyName = "Condfidential"
            privacyDescription = "Limited identity data available to defined person in a certain role"
        case 8:
            privacyName = "Identified"
            privacyDescription = "Data is available with few or no controls"
        case 9:
            privacyName = "Public"
            privacyDescription = "Digital self is livecast, online and cross referenced"
        default:
            privacyName = "Private"
            privacyDescription = "Existence is unknown"
            
        }
    }
    @IBOutlet var statusText: UITextView!

    @IBOutlet var consentText: UITextView!

    @IBOutlet var stateText: UITextView!

}
