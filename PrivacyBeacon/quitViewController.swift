//
//  quitViewController.swift
//  PrivacyBeacon
//
//  Created by Student on 2014-11-22.
//  Copyright (c) 2014 University of Ontario Institute of Technology. All rights reserved.
//

import UIKit
import Darwin

class quitViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        exit(0)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
