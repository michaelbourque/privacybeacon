//
//  scenario1ViewController.swift
//  PrivacyBeacon
//
//  Created by Student on 2014-11-22.
//  Copyright (c) 2014 University of Ontario Institute of Technology. All rights reserved.
//

import UIKit

var scenarioProgress: NSInteger = 0

class scenario1ViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        s1statusText.text = "You will begin this scenario with a Privacy State of \(privacyState) : \(privacyName)\r\rYour Privacy Preferences indicate that you are a \r\(privacyRating). \rThe level of caution will be adjusted accordantly"
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBOutlet var s1statusText: UITextView!



    
}
