//
//  locationViewController.swift
//  PrivacyBeacon
//
//  Created by Student on 2014-11-22.
//  Copyright (c) 2014 University of Ontario Institute of Technology. All rights reserved.
//

import UIKit
import MapKit

class locationViewController: UIViewController {

    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        var location = CLLocationCoordinate2D( latitude: 50.0000,
            longitude: -85.0000
        )
        
        var span = MKCoordinateSpanMake(15,15)
        
        var region = MKCoordinateRegion(center: location, span: span)
        
        var annotation = MKPointAnnotation()
        
        annotation.setCoordinate(location)
        annotation.title="Ontario"
        annotation.subtitle="Canada"
        
        map.mapType = MKMapType.Hybrid
        
        map.setRegion(region, animated: true)
        map.addAnnotation(annotation)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func viewRegulations(sender: AnyObject) {
        var regulationsAlert = UIAlertView()
        regulationsAlert.title = "Applicable Reguations"
        
        regulationsAlert.message =
        "Privacy Act\rFIPPA\rMFIPPA\rPIPEDA\rPHIPA"
        regulationsAlert.addButtonWithTitle("OK")
        regulationsAlert.show()
    }


    
    
}
