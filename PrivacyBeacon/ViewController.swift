//
//  ViewController.swift
//  PrivacyBeacon
//
//  Created by Student on 2014-11-19.
//  Copyright (c) 2014 University of Ontario Institute of Technology. All rights reserved.
//

import UIKit

var surveyComplete: Bool  = false
var tally: NSInteger = 4
var privacyRating: NSString = "Privacy Fundamentalist"
var privacySetting: NSString = "High"
var privacyState: NSInteger = 1
var privacyName: NSString = "Private"
var privacyDescription: NSString = "Existence is unknown"


var collectionConsent: NSString = "None"
var useConsent: NSString = "None"
var disclosureConsent: NSString = "None"

var overrideCount: NSInteger = 0


class ViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet var stateChangeQ1: UILabel!
    @IBOutlet var stateChangeQ2: UILabel!
    @IBOutlet var stateChangeQ3: UILabel!
    @IBOutlet var stateChangeQ4: UILabel!
    
    @IBOutlet var switchQ1: UISwitch!
    @IBOutlet var switchQ2: UISwitch!
    @IBOutlet var switchQ3: UISwitch!
    @IBOutlet var switchQ4: UISwitch!
    

    
    @IBAction func beginQuestionaire(sender: AnyObject) {
        tally = 4
        privacyState = 1
        overrideCount = 0
    }
    @IBAction func valueChangedQ1(sender: AnyObject) {
        if switchQ1.on {
            stateChangeQ1.text = "Yes"
            switchQ1.setOn(true, animated:true)
            println("Yes")
            tally += 1
            println(tally)

            
        } else {
            stateChangeQ1.text = "No"
            switchQ1.setOn(false, animated:true)
            println("No")
            tally -= 1
            println(tally)

        }
    }
    
    @IBAction func valueChangedQ2(sender: AnyObject) {
        if switchQ2.on {
            stateChangeQ2.text = "Yes"
            switchQ2.setOn(true, animated:true)
            println("Yes")
            tally += 1
            println(tally)
            
        } else {
            stateChangeQ2.text = "No"
            switchQ2.setOn(false, animated:true)
            println("No")
            tally -= 1
            println(tally)
        }
    }
    
    @IBAction func valueChangedQ3(sender: AnyObject) {
        if switchQ3.on {
            stateChangeQ3.text = "Yes"
            switchQ3.setOn(true, animated:true)
            println("Yes")
            tally += 1
            println(tally)
            
        } else {
            stateChangeQ3.text = "No"
            switchQ3.setOn(false, animated:true)
            println("No")
            tally -= 1
            println(tally)
        }
    }
    @IBAction func valueChangedQ4(sender: AnyObject) {
        if switchQ4.on {
            stateChangeQ4.text = "Yes"
            switchQ4.setOn(true, animated:true)
            println("Yes")
            tally += 1
            println(tally)
            
        } else {
            stateChangeQ4.text = "No"
            switchQ4.setOn(false, animated:true)
            println("No")
            tally -= 1
            println(tally)
        }
    }
    @IBAction func questionaireDone(sender: AnyObject) {
        getRating()
        surveyComplete = true
        var doneAlert = UIAlertView()
        doneAlert.title = "Survey Complete"
        doneAlert.message = "Your privacy setting is \(privacySetting).\r\r Your privacy rating is\r \(privacyRating)"
        doneAlert.addButtonWithTitle("OK")
        doneAlert.show()
    }
    
    func getRating() {
        if tally > 2 {
            privacySetting=("High")
            privacyRating=("Privacy Fundamentalist")
            collectionConsent = "None"
            useConsent = "None"
            disclosureConsent = "None"
            /*privacyState*/
        } else if tally == 2 {
            privacySetting=("Moderate")
            privacyRating=("Privacy Pragmatic")
            collectionConsent = "Unlimited"
            useConsent = "Limited"
            disclosureConsent = "Limited"
        } else {
            privacySetting=("Low")
            privacyRating=("Privacy Unconcerned")
            collectionConsent = "Unlimited"
            useConsent = "Unlimited"
            disclosureConsent = "Unlimited"

        }

    }

}
